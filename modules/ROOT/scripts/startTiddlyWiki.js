const { execSync } = require("child_process");
const root = execSync("npm root -g")
    .toString()
    .trim();
const path = require('path');
const $tw = require(`${root}/tiddlywiki/boot/boot.js`).TiddlyWiki();

const tiddlyWikiPort = require('../config.json').port;
const username = require('../config.json').username;

// const repoFolder = path.join(path.dirname(__filename), '..');
const tiddlyWikiFolder = path.join(path.dirname(__filename), '..');

// const tiddlyWikiFolder = path.join(repoFolder, wikiFolderName);
const tiddlersFolder = path.join(tiddlyWikiFolder, 'tiddlers');

process.env['TIDDLYWIKI_PLUGIN_PATH'] = `${tiddlyWikiFolder}/plugins`;
process.env['TIDDLYWIKI_THEME_PATH'] = `${tiddlyWikiFolder}/themes`;
// add tiddly filesystem back https://github.com/Jermolene/TiddlyWiki5/issues/4484#issuecomment-596779416
$tw.boot.argv = [
    // '+plugins/tiddlywiki/filesystem',
    // '+plugins/tiddlywiki/tiddlyweb',
    tiddlyWikiFolder,
    '--verbose',
    '--listen',
    `anon-username=${username}`,
    `port=${tiddlyWikiPort}`,
    'host=0.0.0.0',
    'root-tiddler=$:/core/save/lazy-images',
];

try {
    execSync(`find ${tiddlersFolder} -name '*StoryList.tid' -delete`);
} catch (error) {
    console.log(String(error));
}

$tw.boot.boot();