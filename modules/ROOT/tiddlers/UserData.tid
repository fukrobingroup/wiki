created: 20220602012234216
creator: fukrobin
modified: 20220602012619202
modifier: fukrobin
tags: EC2
title: UserData
type: text/vnd.tiddlywiki

!! Utilize  `UserData` run a script with every restart

<$linkpreview url="https://aws.amazon.com/premiumsupport/knowledge-center/execute-user-data-ec2/" />

<<alert info "By default, user data scripts and cloud-init directives run only during the first boot cycle when an EC2 instance is launched">>

The following example is a shell script that writes ''"Hello World"'' to a testfile.txt file in a ''/tmp'' directory.

```
Content-Type: multipart/mixed; boundary="//"
MIME-Version: 1.0

--//
Content-Type: text/cloud-config; charset="us-ascii"
MIME-Version: 1.0
Content-Transfer-Encoding: 7bit
Content-Disposition: attachment; filename="cloud-config.txt"

#cloud-config
cloud_final_modules:
- [scripts-user, always]

--//
Content-Type: text/x-shellscript; charset="us-ascii"
MIME-Version: 1.0
Content-Transfer-Encoding: 7bit
Content-Disposition: attachment; filename="userdata.txt"

#!/bin/bash
/bin/echo "Hello World" >> /tmp/testfile.txt
--//--
```
