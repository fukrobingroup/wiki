created: 20220421095018728
creator: 2213662140@qq.com
modified: 20220424062844804
modifier: 2213662140@qq.com
tags: SSL
title: Certificate
type: text/vnd.tiddlywiki

<<alert info """
详情参考：

* <<html-link "Tutorial: Understanding X.509 Public Key Certificates" "https://docs.microsoft.com/en-us/azure/iot-hub/tutorial-x509-certificates">>
* [[PCKS12 & X.509 & PEM]]
* [[What is X.509?]]
""" >>

有几种不同类型的 SSL 证书。一个证书可以应用于一个或多个网站，具体取决于类型：

;单域
:单域 SSL 证书仅适用于一个域（“域”是网站的名称，例如 www.cloudflare.com）。
;通配符
:与单域证书一样，通配符 SSL 证书仅适用于一个域。但是，它也包括该域的子域。例如，通配符证书可以覆盖 www.cloudflare.com、blog.cloudflare.com，和 developers.cloudflare.com，而单域证书只能覆盖第一个。
;多域
:顾名思义，多域 SSL 证书可以应用于多个不相关的域。

SSL 证书还具有不同的验证级别。验证级别就像背景检查一样，并且级别会根据检查的彻底性而变化。

;域验证
:这是最严格的验证级别，也是最便宜的级别。企业只需要证明他们控制着域。
;组织验证
:这是一个需要亲力亲为的过程：证书机构直接联系请求证书的人员或企业。这些证书更受用户信赖。
;扩展验证
:在发出 SSL 证书之前，需要对组织进行全面的背景检查。
