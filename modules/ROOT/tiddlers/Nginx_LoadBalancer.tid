caption: AWS LoadBalancer
created: 20220418140301071
creator: 2213662140@qq.com
modified: 20220522133934012
modifier: fukrobin
tags: Nginx
title: Nginx/LoadBalancer
type: text/vnd.tiddlywiki

<$linkpreview url="https://nginx.org/en/docs/http/load_balancing.html" />
<$linkpreview url="https://nginx.org/en/docs/http/ngx_http_upstream_module.html#upstream" />

Nginx 可以作为一个非常有效的 HTTP 负载平衡器来将流量分配到多个应用程序服务器，并使用 nginx 提高 Web 应用程序的性能、可伸缩性和可靠性。

!! 负载均衡的方法

* `round-robin`(默认值)— 在配置的服务器中循环
* `least-connected`— 选择当前活动链接最少的服务器
* `ip-hash`—hash 请求的IP地址，以确定访问哪个服务器（此IP地址之后的请求将在相同的服务器）

### Round-robin

Nginx 默认的负载均衡方法，使用**加权轮询**的方式：

```nginx
upstream backend {
    server backend1.example.com weight=5;
    server 127.0.0.1:8080       max_fails=3 fail_timeout=30s;
    server unix:/tmp/backend3;

    server backup1.example.com  backup;
}
```
上面的例子中每 7 个请求将有 5 个被发送至 `backend1.example.com`。如果请求与服务器通信失败，将被发送至下一个服务器，直到尝试所有正常运行的服务器。如果无法从任何服务器获得成功响应，则客户端将收到与最后一个服务器通信的结果。

### least-connected

将请求发生至平均响应时间最短且活动连接数最少的服务器，同时考虑服务器的权重

```nginx
upstream myapp1 {
	least_conn;
	server srv1.example.com;
	server srv2.example.com;
	server srv3.example.com;
}
```

### ip hash

通过对客户端请求的 IP 地址进行哈希运算，选择服务器。此方法保证了统一客户端始终访问相同的服务器（除非服务器不可用）

```nginx
upstream myapp1 {
    ip_hash;
    server srv1.example.com;
    server srv2.example.com;
    server srv3.example.com;
}
```
