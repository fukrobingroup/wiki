运行 Application Version 的 AWS 资源集合。每个环境只能运行一个Application Version

创建环境时，Elastic Beanstalk 会预配置运行指定的Application Version所需的资源

## Environment Configurations

标识一组参数和配置，这些参数和配置用于定义环境及其相关资源的行为方式。当您更新环境的配置设置时，Elastic Beanstalk 会自动将更改应用到现有资源，或者删除现有资源并部署新资源（取决于更改的类型）。

### Saved configuration

作为*模板*使用，可以作为创建环境配置的起点。API、Amazon CLI将saved configuration叫做configuration templete

可以创建、修改并应用到envrionment，

## Environment Tier

用于指定Environment运行的应用程序类型，Elastic Beanstalk 根据此确定需要预配置的资源

*   Web server  environment：为HTTP轻轻提供服务的环境

*   Worker environment：从AWS SQS队列中提取任务的后端环境

![select_environment_tier](Assets/ElasticBeanstalk/select_environment_tier.png)

#### Web server  environment

![Web server  environment example Architecture](https://docs.amazonaws.cn/elasticbeanstalk/latest/dg/images/aeb-architecture2.png "Web server  environment example Architecture")

#### Worker environment