created: 20220514102309645
creator: fukrobin
description: :You Ain’t Gonna Need It
modified: 20220514111646055
modifier: fukrobin
tags: 设计原则
title: YAGNI
type: text/vnd.tiddlywiki

http://martinfowler.com/bliki/Yagni.html

<<alert primary """Yagni only applies to capabilities built into the software to support a presumptive feature, it does not apply to effort to make the software easier to modify.
""" >>

<<alert warning "Yagni 通常仅使用于将增加软件复杂性的功能，因为可能是无用的。如果引入的新功能对复杂性没有影响，那么就没有强调 `Yagni` 的必要">>

You Ain’t Gonna Need It(你不会需要它), 旨在消除所有冗余代码,并''专注于当前而不是未来的功能''

* 将时间花费在''推定''的功能、特性上会显著增加成本：分析、编码、测试
* 即使对需求的理解完全正确，即使在这种情况下，也会导致额外的成本：
** ''cost of delay''：在开发推定的功能期间，我们如果开发能够带来实际收益的功能，那么成本将是这此周期内的能够获得的收益
** ''cost of carry''：添加推定功能将会增加项目的复杂性，这会导致项目的修改、调试变得更加困难，而增加了其他功能的成本。在推定功能有用之前的所有功能的开发都会增加成本。如果之后确定不需要此功能，我们甚至将承担删除（如果需要）的成本。

仅当代码易于更改时, Yagni 才是一种可行的策略，因此对代码的重构并不违反 Yagni，因为这是为了代码的可塑性