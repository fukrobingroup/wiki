created: 20220428000557528
creator: 2213662140@qq.com
modified: 20220502050534486
modifier: 2213662140@qq.com
tags: OpenGL
title: OpenGL/CullFace
type: text/vnd.tiddlywiki

`CullFace` 的想法很简单，对于一个正方体来说，无论你身处什么方向，''最多''只能看到正方体的三个面，因此对于看不到的其他面实际上没有必要进行实际的渲染！

OpenGL 通过[[OpenGL/CullFace/顶点环绕顺序]]判断一个面是否能被看到

!! 函数

`glCullFace` 函数有三个可用的选项：

* `GL_BACK`：只剔除背向面。
* `GL_FRONT`：只剔除正向面。
* `GL_FRONT_AND_BACK`：剔除正向面和背向面。

`glFrontFace` 定义环绕顺序，OpenGL 通过此判断面是否是正向的：

* `GL_CCW`： 逆时针
* `GL_CW`: 顺时针

<$details summary="基本使用">

```c
glEnable(GL_CULL_FACE);
glCullFace(GL_FRONT);
glFrontFace(GL_CCW);
```

</$details>