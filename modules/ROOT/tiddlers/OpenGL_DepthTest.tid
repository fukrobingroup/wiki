created: 20220418052353629
creator: 2213662140@qq.com
modified: 20220502050457318
modifier: 2213662140@qq.com
stream-list: DepthTest/20220418052630247
stream-type: default
tags: OpenGL
title: OpenGL/DepthTest
type: text/vnd.tiddlywiki

类似于//颜色缓冲//，在每个片段中存储深度信息（深度值），并且（通常）和颜色缓冲有着一样的宽度和高度。深度缓冲是由窗口系统自动创建的，它会以16、24或32位float的形式储存它的深度值。在大部分的系统中，深度缓冲的精度都是24位的。

当启用深度测试时，OpenGL 会将一个片段的深度值与深度缓冲中的内容进行对比，如果测试通过，缓冲中的深度值被更新，如果测试失败，片段被丢弃。

深度测试运行在片段着色器之后（以及 OpenGL/StencilTest 运行之后）,可以通过 GLSL 的内建变量 `gl_FragCoord` 访问深度值，即Z分量。`gl_FragCoord` 的X、Y分量代表了片段的屏幕空间坐标（其中(0, 0)位于左下角）。

<$details summary="深度函数">

```c
void glDepthFunc(GLenum func);
```
''func'': [[OpenGL/Condition enum]]
</$details>
<$details summary="基本使用">

```java
glEnable(GL_DEPTH_TEST);
glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
glDepthMask(GL_FALSE);
// 禁用深度测试
glDisable(GL_DEPTH_TEST);
```
</$details>

<<alert info """什么是 [[DepthFighting]]？""">>
