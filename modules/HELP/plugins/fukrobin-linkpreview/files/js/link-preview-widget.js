/*\
title: $:/plugins/fukrobin/link-preview/widget.js
type: application/javascript
module-type: widget

Text node widget

\*/
(function () {

    /*jslint node: true, browser: true */
    /*global $tw: false */
    "use strict";

    var Widget = require("$:/core/modules/widgets/widget.js").widget;

    var LinkPreviewWidget = function (parseTreeNode, options) {
        this.initialise(parseTreeNode, options);
        this.logger = new $tw.utils.Logger("LinkPreviewWidget");
        this.defaultTemplate = "$:/plugins/fukrobin/link-preview/template/default"
    };

    /*
    Inherit from the base widget class
    */
    LinkPreviewWidget.prototype = new Widget();

    /*
    Render this widget into the DOM
    */
    LinkPreviewWidget.prototype.render = function (parent, nextSibling) {
        this.parentDomNode = parent;
        this._nextSibling = nextSibling;
        // Compute the current values of the attributes of the widget. 
        // Returns a hashmap of the names of the attributes that have changed
        this.computeAttributes();
        this.execute();
    };

    function validateUrl(url) {
        const url_regex = /^(?:(?:(?:https?|ftp):)?\/\/)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:[/?#]\S*)?$/i;

        return url_regex.test(url);
    }

    /*
    Compute the internal state of the widget
    */
    LinkPreviewWidget.prototype.execute = function () {
        let url = this.getAttribute("url");
        let self = this;
        this.template = this.getAttribute("template");
        this.getLinkPreview(url).then(() => {
            this.makeChildWidgets(this.makeTemplate());
            this.renderChildren(self.parentDomNode, self._nextSibling);
        });
    };

    LinkPreviewWidget.prototype.getLinkPreview = function (url) {
        return new Promise((resolve, reject) => {
            this.setVariable("link-preview-url", url);
            if (!validateUrl(url)) {
                resolve()
                return;
            }

            let self = this;
            this.logger.log("Get link preview: " + url);
            $tw.utils.httpRequest({
                url: window.location.origin + "/custom/linkpreview?url=" + encodeURIComponent(url),
                callback: function (err, data) {
                    if (err) {
                        resolve();
                        return;
                    }
                    // Decode the status JSON
                    var json = null;
                    try {
                        json = JSON.parse(data);
                    } catch (e) {
                    }
                    if (json) {
                        self.logger.log("response: ", data);
                        self.setVariable("link-preview-title", json.title);
                        self.setVariable("link-preview-description", json.description);
                        var image = "";
                        if (json.images instanceof Array) image = json.images[0];
                        var favicon = "";
                        if (json.favicons instanceof Array) favicon = json.favicons[0];
                        self.setVariable("link-preview-image", image);
                        self.setVariable("link-preview-favicon", favicon);
                    }
                    resolve();
                }
            })
        });
    }

    LinkPreviewWidget.prototype.makeTemplate = function () {
        var template = this.template || this.defaultTemplate;
        return [{type: "transclude", attributes: {tiddler: {type: "string", value: template}}}];
    }

    /*
    Selectively refreshes the widget if needed. Returns true if the widget or any of its children needed re-rendering
    */
    LinkPreviewWidget.prototype.refresh = function (changedTiddlers) {
        var changedAttributes = this.computeAttributes();
        if (changedAttributes.url || changedAttributes.template) {
            this.refreshSelf();
            return true;
        } else {
            return false;
        }
    };

    exports.linkpreview = LinkPreviewWidget;

})();