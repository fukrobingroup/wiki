$tw.config = {
  fileExtensionInfo: Object.create(null),
  contentTypeInfo: Object.create(null)
}

$tw.utils.registerFileType("application/x-ptiddlers", "utf8", ".ptids");