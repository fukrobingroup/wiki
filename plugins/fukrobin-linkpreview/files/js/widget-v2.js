/*\
title: $:/plugins/fukrobin/link-preview/widget.js
type: application/javascript
module-type: widget

Link preview widget

\*/
(function () {

  /*jslint node: true, browser: true */
  /*global $tw: false */
  "use strict";

  var Widget = require("$:/core/modules/widgets/widget.js").widget;

  var LinkPreviewWidget = function (parseTreeNode, options) {
    this.initialise(parseTreeNode, options);
    this.logger = new $tw.utils.Logger("LinkPreviewWidget");
    this.defaultTemplate = "$:/plugins/fukrobin/link-preview/template/default";
    this.defaultLoadingTemplate = "$:/plugins/fukrobin/link-preview/template/loading";
    this.tempCacheTitle = "$:/temp/link-preview/cache";
    this.linkpreviewApi = window.location.origin + "/custom/linkpreview?url="
  };

  /*
  Inherit from the base widget class
  */
  LinkPreviewWidget.prototype = new Widget();

  /*
  Render this widget into the DOM
  */
  LinkPreviewWidget.prototype.render = function (parent, nextSibling) {
    this.parentDomNode = parent;
    this.containerDomNode = this.document.createElement("div");
    this.containerDomNode.setAttribute("class", "linkpreview");

    this.computeAttributes();
    this.execute();

    parent.insertBefore(this.containerDomNode, nextSibling);
    this.renderChildren(this.containerDomNode, null);
    this.renderPreview(this.containerDomNode);
    this.domNodes.push(this.containerDomNode);
  };

  function validateUrl(url) {
    const url_regex = /^(?:(?:(?:https?|ftp):)?\/\/)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z¡-￿0-9]-*)*[a-z¡-￿0-9]+)(?:\.(?:[a-z¡-￿0-9]-*)*[a-z¡-￿0-9]+)*(?:\.(?:[a-z¡-￿]{2,})))(?::\d{2,5})?(?:[/?#]\S*)?$/i;

    return url_regex.test(url);
  }

  /*
  Compute the internal state of the widget
  */
  LinkPreviewWidget.prototype.execute = function () {
    this.url = this.getAttribute("url");

    this.template = this.getAttribute("template") || this.defaultTemplate;
    this.loadingTemplate = this.getAttribute("loadingTemplate") || this.defaultLoadingTemplate;
    // this.containerDomNode.innerHTML = this.renderLoadingBox();
    // this.makeChildWidgets(this.makeTemplate());
    this.makeChildWidgets();
  };

  LinkPreviewWidget.prototype.renderPreview = function (parentDomNoe) {
    this.getLinkPreview(this.url)
        .then(data => this.setVariables(data))
        .then(data => this.cacheSuccessResponse(data))
        .then(data => this.validateResponse(data))
        .catch((err) => {
          console.error(err);
          var stateTiddler = this.getVariable("stateTiddler");
          if (stateTiddler) {
            $tw.wiki.addTiddler({
              title: stateTiddler,
              text: "hide"
            });
            this.setVariables("link-preview-error", err.toString());
          }
        })
        .finally(() => {
          this.makeChildWidget(this.makeTemplate()).render(parentDomNoe, null);
        });
  }

  LinkPreviewWidget.prototype.renderLoadingBox = function () {
    return $tw.wiki.getTiddler(this.loadingTemplate).fields.text;
  }

  LinkPreviewWidget.prototype.validateResponse = function (data) {
    if (!data.title || !data.description) {
      throw new Error("Response missing component.")
    }
    return data;
  }

  LinkPreviewWidget.prototype.getLinkPreview = function (url) {
    if (url) url = url.trim();
    return new Promise((resolve, reject) => {
      this.setVariable("link-preview-url", url);
      if (!validateUrl(url)) {
        reject("invalid url");
        return;
      }

      var encodedUrl = encodeURIComponent(url);
      var cachedData = this.getLinkPreviewFromCache(encodedUrl);
      if (cachedData) {
        resolve(cachedData);
      } else {
        this.logger.log("Get link preview: " + url);
        fetch(this.linkpreviewApi + encodedUrl, {mode: 'no-cors'})
            .then(response => response.json())
            .then(data => resolve(data));
      }
    });
  }

  LinkPreviewWidget.prototype.setVariables = function (response) {
    if (response.title) this.setVariable("link-preview-title", response.title);
    if (response.description) this.setVariable("link-preview-description", response.description);
    if (response.image) this.setVariable("link-preview-image", response.image);
    if (response.logo) this.setVariable("link-preview-favicon", response.logo);
    return response;
  }

  LinkPreviewWidget.prototype.getLinkPreviewFromCache = function (url) {
    var cachedData = this.wiki.getTiddlerData(this.tempCacheTitle, {});
    return cachedData[url];
  }

  LinkPreviewWidget.prototype.cacheSuccessResponse = function (response) {
    var cached = this.wiki.getTiddlerData(this.tempCacheTitle, {});
    var data = {};
    if (response.title) data["title"] = response.title;
    if (response.description) data["description"] = response.description;
    if (response.image) data["image"] = response.image;
    if (response.logo) data["logo"] = response.logo;
    if (Object.keys(data).length > 0) {
      cached[encodeURIComponent(this.url)] = data;
      this.wiki.setTiddlerData(this.tempCacheTitle, cached);
    }
    return response;
  }

  LinkPreviewWidget.prototype.makeTemplate = function () {
    return {type: "transclude", attributes: {tiddler: {type: "string", value: this.template}}};
  }

  LinkPreviewWidget.prototype.renderUrl = function () {
    // var domNode = this.document.createElementNS("http://www.w3.org/1999/xhtml", "a");
    // domNode.setAttribute("target", "_blank");
    // domNode.setAttribute("href", this.url);
    // domNode.innerHTML = this.url;
    var attributes = {
      "href": {
        "name": "href",
        "type": "string",
        "value": this.url
      },
      "target": {
        "name": "target",
        "type": "string",
        "value": "_blank"
      }
    }
    return {
      "type": "element",
      "attributes": attributes,
      "tag": "a",
      "children": [
        {
          "type": "text",
          "text": this.url
        }
      ]
    };
  }

  /*
  Selectively refreshes the widget if needed. Returns true if the widget or any of its children needed re-rendering
  */
  LinkPreviewWidget.prototype.refresh = function (changedTiddlers) {
    var changedAttributes = this.computeAttributes();
    if (changedAttributes.url || changedAttributes.template) {
      this.refreshSelf();
      return true;
    } else {
      return false;
    }
  };

  exports.linkpreview = LinkPreviewWidget;

})();