/*\
title: $:/plugins/fukrobin/link-preview/api.js
type: application/javascript
module-type: route

GET /linkpreview?url=https%3A%2F%2Fgoogle.com

\*/
(function () {

  /*jslint node: true, browser: true */
  /*global $tw: false */
  "use strict";

  const axios = require('axios').default;
  const HttpsProxyAgent = require('https-proxy-agent');

  exports.method = "GET";

  exports.path = /^\/custom\/linkpreview$/;

  function validate_url(url) {
    if (!url) {
      return false;
    }

    const url_regex = /^(?:(?:(?:https?|ftp):)?\/\/)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z¡-￿0-9]-*)*[a-z¡-￿0-9]+)(?:\.(?:[a-z¡-￿0-9]-*)*[a-z¡-￿0-9]+)*(?:\.(?:[a-z¡-￿]{2,})))(?::\d{2,5})?(?:[/?#]\S*)?$/i;

    return url_regex.test(url);
  }

  exports.handler = function (request, response, state) {
    let parameters = state.queryParameters;
    if (!(parameters && validate_url(parameters.url))) {
      state.sendResponse(404, {"Content-Type": "application/json"}, JSON.stringify({url: parameters.url}), "utf8");
      return;
    }

    var URL = `https://www.linkpreview.io/api/one-link?website=${parameters.url}`;
    var agent = null;
    if (process.env['HTTP_PROXY']) {
      agent = new HttpsProxyAgent.HttpsProxyAgent(process.env['HTTP_PROXY']);
    }

    axios.get(URL, {
      baseURL: "",
      validateStatus: function (status) {
        return status >= 200 && status < 300; // default
      },
      proxy: false,
      httpsAgent: agent
    }).then(response => {
      state.sendResponse(200, {"Content-Type": "application/json"}, JSON.stringify(response.data), "utf8");
    }).catch((error) => {
      if (error.response) {
        console.log(error.response.data);
      }
      console.log(error.config);
      state.sendResponse(404, {"Content-Type": "application/json"}, JSON.stringify({url: parameters.url}), "utf8");
    });
  };
}());