/*\
title: $:/core/modules/startup/favicon.js
type: application/javascript
module-type: startup

Favicon handling

\*/
(function () {

  /*jslint node: true, browser: true */
  /*global $tw: false */
  "use strict";

// Export name and synchronous status
  exports.name = "register-pTiddlers";
  exports.platforms = ["browser"];
  exports.synchronous = true;

  exports.startup = function () {
    console.log("Register a custom file type");
    $tw.utils.registerFileType("application/x-ptiddlers", "utf8", ".ptids");
  };

})();
