/*\
title: $:/plugins/fukrobin/ptiddlers/deserializer.js
type: application/javascript
module-type: tiddlerdeserializer

\*/
(function () {

  /*jslint node: true, browser: true */
  /*global $tw: false */
  "use strict";

  /**
   * Tiddler 必须包含 title 字段，否则将被忽略
   *
   * @param {string} text
   * @param {Object} fields
   * @param {string} baseTitle
   */
  function defaultTiddlerDeserializer(text, fields, baseTitle) {
    var split = text.split(/\r?\n\r?\n/mg);
    if (split.length >= 1) {
      fields = $tw.utils.parseFields(split[0], fields);
    }
    if (split.length >= 2) {
      fields.text = split.slice(1).join("\n\n");
    }
    fields.title = (baseTitle || "") + fields.title;
    if (fields.title) {
      return fields;
    }
  }

  exports["application/x-ptiddlers"] = function (text, fields) {
    var tiddlers = [];
    var areas = text.split(/###/mg);
    var baseTitle = "",
        formattedBaseTitle = "",
        creator = null,
        modifier = null;
    for (let i = 0; i < areas.length; i++) {
      var tiddler = defaultTiddlerDeserializer(areas[i]);
      if (!tiddler) continue;
      if (!tiddler && i === 0) break;

      if (i === 0) {
        baseTitle = tiddler.title;
        creator = tiddler.creator;
        modifier = tiddler.modifier;
        formattedBaseTitle = removeEndSlash(baseTitle);
        tiddler.type = "text/vnd.tiddlywiki";
      } else {
        tiddler.title = (baseTitle || "") + tiddler.title;
        var tags = tiddler.tags || "";
        if (creator && !tiddler.creator) tiddler.creator = creator;
        if (modifier && !tiddler.modifier) tiddler.modifier = modifier;
        if (tags.indexOf(baseTitle) < 0) tiddler.tags = tags.trimEnd().concat(" " + formattedBaseTitle);
      }
      tiddler.title = removeEndSlash(tiddler.title);
      tiddlers.push(tiddler, {});
    }
    return tiddlers;
  }

  function removeEndSlash(title) {
    if (title.endsWith("/")) {
      return title.substring(0, title.indexOf('/'));
    }
    return title;
  }

})();