/*\
title: $:/plugins/fukrobin/tagged-this/macro
type: application/javascript
module-type: macro

Macro to return a formatted version of the current time
\*/
(function () {

    /*jslint node: true, browser: true */
    /*global $tw: false */
    "use strict";

    /*
    Information about this macro
    */

    exports.name = "tagged-this";

    exports.params = [
        {name: "title"},
        {name: "numCols"}
    ];

    function createElement(parent, tag) {
        var el = document.createElement(tag);
        if (parent) parent.appendChild(el);
        return el;
    }

    function makeColumns(orign, numCols) {
        let size = orign.length;
        let colSize = size / numCols;
        let remainder = size % numCols;

        var upperColsize = colSize;
        var lowerColsize = colSize;

        if (colSize !== Math.floor(colSize)) {
            // it's not an exact fit so..
            upperColsize = Math.floor(colSize) + 1;
            lowerColsize = Math.floor(colSize);
        }

        var output = [];
        var c = 0;
        for (var j = 0; j < numCols; j++) {
            var singleCol = [];
            var thisSize = j < remainder ? upperColsize : lowerColsize;
            for (var i = 0; i < thisSize; i++)
                singleCol.push(orign[c++]);
            output.push(singleCol);
        }

        return output;
    }

    function drawTable(columns) {
        var tbStart = "<table><tbody><tr>"
        var tbEnd = "</tr></tbody></table>"
        var tbContent = ""

        for (var j = 0; j < columns.length; j++) {
            var colOutput = "";
            for (var i = 0; i < columns[j].length; i++) {
                colOutput += `<li><$link tooltip=<<tt_link_tooltip>> to="${columns[j][i]}" /><$macrocall $name="tt_toc" tag="${columns[j][i]}"/></li>`;
            }
            tbContent += `<td class="tagglyTagging"><ul>${colOutput}</ul></td>`;
        }
        return tbStart + tbContent + tbEnd;
    }

    /*
    Run the macro
    */
    exports.run = function (title, numCols) {
        title = title || this.getVariable("currentTiddler");
        numCols = numCols || 4;

        var tiddlers = $tw.wiki.getTiddlersWithTag(title);

        return drawTable(makeColumns(tiddlers, numCols));
    };

})();