if (process.argv.length < 3) {
  console.error("Missing arguments, please special module name!")
}

const fs = require("fs");
const path = require('path');
const {execSync} = require("child_process");
const $tw = require('tiddlywiki/boot/boot.js').TiddlyWiki();

let moduleName = process.argv[2]
let tiddlyWikiFolder = path.join(path.dirname(__filename), `../modules/${moduleName}`)
const tiddlersFolder = path.join(tiddlyWikiFolder, 'tiddlers');

const config = require(`${tiddlyWikiFolder}/config.json`);
const tiddlyWikiPort = config.port;
const username = config.username;

process.env['TIDDLYWIKI_PLUGIN_PATH'] = `${tiddlyWikiFolder}/plugins`;
process.env['TIDDLYWIKI_THEME_PATH'] = `${tiddlyWikiFolder}/themes`;

function extraPlugins() {
  var defaultPlugins = [
    '+plugins/tiddlywiki/filesystem',
    '+plugins/tiddlywiki/tiddlyweb']
  var commonPlugins = fs.readdirSync("./plugins").map(path => "++./plugins/" + path);
  return defaultPlugins.concat(commonPlugins);
}

var plugins = extraPlugins();

$tw.boot.argv = plugins.concat([
  tiddlyWikiFolder,
  '--verbose',
  '--listen',
  `anon-username=${username}`,
  `port=${tiddlyWikiPort}`,
  'host=127.0.0.1',
  'root-tiddler=$:/core/save/lazy-images',
]);

try {
  execSync(`find ${tiddlersFolder} -name '*StoryList.tid' -delete`);
} catch (error) {
  console.log(String(error));
}

$tw.config = {
  fileExtensionInfo: Object.create(null),
  contentTypeInfo: Object.create(null)
}

$tw.utils.registerFileType("application/x-ptiddlers", "utf8", ".ptids");

$tw.boot.boot();